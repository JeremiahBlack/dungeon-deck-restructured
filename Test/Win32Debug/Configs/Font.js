{
"Font":[
{
"charID":33,
"x":43,
"y":12,
"width":20,
"height":70,
"descender":0,
"raise":1
},
{
"charID":34,
"x":138,
"y":16,
"width":34,
"height":22,
"descender":0,
"raise":45
},
{
"charID":35,
"x":222,
"y":12,
"width":70,
"height":70,
"descender":0,
"raise":1
},
{
"charID":36,
"x":336,
"y":6,
"width":47,
"height":77,
"descender":0,
"raise":0
},
{
"charID":37,
"x":440,
"y":15,
"width":42,
"height":68,
"descender":0,
"raise":0
},
{
"charID":38,
"x":536,
"y":13,
"width":55,
"height":69,
"descender":0,
"raise":1
},
{
"charID":39,
"x":661,
"y":16,
"width":8,
"height":22,
"descender":0,
"raise":45
},
{
"charID":40,
"x":754,
"y":10,
"width":24,
"height":73,
"descender":0,
"raise":0
},
{
"charID":41,
"x":857,
"y":10,
"width":24,
"height":73,
"descender":0,
"raise":0
},
{
"charID":42,
"x":958,
"y":15,
"width":26,
"height":25,
"descender":0,
"raise":43
},
{
"charID":43,
"x":29,
"y":121,
"width":50,
"height":47,
"descender":0,
"raise":17
},
{
"charID":44,
"x":151,
"y":168,
"width":8,
"height":22,
"descender":5,
"raise":0
},
{
"charID":45,
"x":234,
"y":139,
"width":48,
"height":8,
"descender":0,
"raise":38
},
{
"charID":46,
"x":351,
"y":168,
"width":16,
"height":16,
"descender":0,
"raise":1
},
{
"charID":47,
"x":440,
"y":117,
"width":42,
"height":68,
"descender":0,
"raise":0
},
{
"charID":48,
"x":538,
"y":108,
"width":50,
"height":78,
"descender":1,
"raise":0
},
{
"charID":49,
"x":645,
"y":107,
"width":41,
"height":78,
"descender":0,
"raise":0
},
{
"charID":50,
"x":742,
"y":107,
"width":48,
"height":78,
"descender":0,
"raise":0
},
{
"charID":51,
"x":846,
"y":107,
"width":41,
"height":78,
"descender":0,
"raise":0
},
{
"charID":52,
"x":952,
"y":109,
"width":34,
"height":76,
"descender":0,
"raise":0
},
{
"charID":53,
"x":35,
"y":210,
"width":44,
"height":76,
"descender":0,
"raise":1
},
{
"charID":54,
"x":134,
"y":211,
"width":41,
"height":75,
"descender":0,
"raise":1
},
{
"charID":55,
"x":231,
"y":211,
"width":55,
"height":76,
"descender":0,
"raise":0
},
{
"charID":56,
"x":336,
"y":209,
"width":46,
"height":80,
"descender":2,
"raise":0
},
{
"charID":57,
"x":438,
"y":211,
"width":46,
"height":76,
"descender":0,
"raise":0
},
{
"charID":58,
"x":555,
"y":218,
"width":16,
"height":58,
"descender":0,
"raise":11
},
{
"charID":59,
"x":657,
"y":218,
"width":16,
"height":64,
"descender":0,
"raise":5
},
{
"charID":60,
"x":750,
"y":218,
"width":35,
"height":55,
"descender":0,
"raise":14
},
{
"charID":61,
"x":851,
"y":230,
"width":42,
"height":27,
"descender":0,
"raise":30
},
{
"charID":62,
"x":954,
"y":218,
"width":35,
"height":55,
"descender":0,
"raise":14
},
{
"charID":63,
"x":34,
"y":316,
"width":37,
"height":72,
"descender":0,
"raise":1
},
{
"charID":64,
"x":117,
"y":315,
"width":70,
"height":75,
"descender":1,
"raise":0
},
{
"charID":65,
"x":227,
"y":312,
"width":60,
"height":76,
"descender":0,
"raise":1
},
{
"charID":66,
"x":329,
"y":312,
"width":61,
"height":77,
"descender":0,
"raise":0
},
{
"charID":67,
"x":429,
"y":311,
"width":66,
"height":79,
"descender":1,
"raise":0
},
{
"charID":68,
"x":534,
"y":312,
"width":57,
"height":77,
"descender":0,
"raise":0
},
{
"charID":69,
"x":636,
"y":312,
"width":58,
"height":77,
"descender":0,
"raise":0
},
{
"charID":70,
"x":738,
"y":312,
"width":54,
"height":77,
"descender":0,
"raise":0
},
{
"charID":71,
"x":839,
"y":312,
"width":60,
"height":76,
"descender":0,
"raise":1
},
{
"charID":72,
"x":941,
"y":312,
"width":60,
"height":77,
"descender":0,
"raise":0
},
{
"charID":73,
"x":24,
"y":414,
"width":59,
"height":77,
"descender":0,
"raise":0
},
{
"charID":74,
"x":125,
"y":414,
"width":59,
"height":77,
"descender":0,
"raise":0
},
{
"charID":75,
"x":233,
"y":414,
"width":50,
"height":78,
"descender":1,
"raise":0
},
{
"charID":76,
"x":332,
"y":414,
"width":52,
"height":77,
"descender":0,
"raise":0
},
{
"charID":77,
"x":431,
"y":417,
"width":60,
"height":74,
"descender":0,
"raise":0
},
{
"charID":78,
"x":533,
"y":417,
"width":60,
"height":74,
"descender":0,
"raise":0
},
{
"charID":79,
"x":636,
"y":415,
"width":57,
"height":77,
"descender":1,
"raise":0
},
{
"charID":80,
"x":741,
"y":414,
"width":52,
"height":77,
"descender":0,
"raise":0
},
{
"charID":81,
"x":840,
"y":414,
"width":57,
"height":77,
"descender":0,
"raise":0
},
{
"charID":82,
"x":945,
"y":414,
"width":53,
"height":77,
"descender":0,
"raise":0
},
{
"charID":83,
"x":26,
"y":516,
"width":57,
"height":78,
"descender":1,
"raise":0
},
{
"charID":84,
"x":125,
"y":515,
"width":59,
"height":79,
"descender":1,
"raise":0
},
{
"charID":85,
"x":229,
"y":516,
"width":58,
"height":78,
"descender":1,
"raise":0
},
{
"charID":86,
"x":331,
"y":516,
"width":56,
"height":78,
"descender":1,
"raise":0
},
{
"charID":87,
"x":431,
"y":516,
"width":60,
"height":78,
"descender":1,
"raise":0
},
{
"charID":88,
"x":534,
"y":515,
"width":59,
"height":79,
"descender":1,
"raise":0
},
{
"charID":89,
"x":636,
"y":515,
"width":59,
"height":78,
"descender":0,
"raise":0
},
{
"charID":90,
"x":737,
"y":516,
"width":60,
"height":77,
"descender":0,
"raise":0
},
{
"charID":91,
"x":852,
"y":516,
"width":35,
"height":77,
"descender":0,
"raise":0
},
{
"charID":92,
"x":950,
"y":524,
"width":42,
"height":68,
"descender":0,
"raise":1
},
{
"charID":93,
"x":36,
"y":618,
"width":35,
"height":77,
"descender":0,
"raise":0
},
{
"charID":94,
"x":136,
"y":619,
"width":38,
"height":29,
"descender":0,
"raise":47
},
{
"charID":95,
"x":232,
"y":685,
"width":50,
"height":10,
"descender":0,
"raise":0
},
{
"charID":96,
"x":350,
"y":620,
"width":18,
"height":20,
"descender":0,
"raise":55
},
{
"charID":97,
"x":440,
"y":653,
"width":49,
"height":42,
"descender":0,
"raise":0
},
{
"charID":98,
"x":540,
"y":617,
"width":48,
"height":78,
"descender":0,
"raise":0
},
{
"charID":99,
"x":642,
"y":652,
"width":45,
"height":43,
"descender":0,
"raise":0
},
{
"charID":100,
"x":743,
"y":617,
"width":48,
"height":78,
"descender":0,
"raise":0
},
{
"charID":101,
"x":850,
"y":652,
"width":41,
"height":43,
"descender":0,
"raise":0
},
{
"charID":102,
"x":947,
"y":618,
"width":47,
"height":78,
"descender":1,
"raise":0
},
{
"charID":103,
"x":30,
"y":756,
"width":45,
"height":58,
"descender":17,
"raise":0
},
{
"charID":104,
"x":135,
"y":719,
"width":44,
"height":79,
"descender":1,
"raise":0
},
{
"charID":105,
"x":251,
"y":750,
"width":12,
"height":47,
"descender":0,
"raise":0
},
{
"charID":106,
"x":345,
"y":748,
"width":28,
"height":66,
"descender":17,
"raise":0
},
{
"charID":107,
"x":437,
"y":720,
"width":48,
"height":79,
"descender":2,
"raise":0
},
{
"charID":108,
"x":558,
"y":720,
"width":10,
"height":77,
"descender":0,
"raise":0
},
{
"charID":109,
"x":643,
"y":754,
"width":46,
"height":44,
"descender":1,
"raise":0
},
{
"charID":110,
"x":745,
"y":752,
"width":46,
"height":46,
"descender":1,
"raise":0
},
{
"charID":111,
"x":844,
"y":750,
"width":49,
"height":48,
"descender":1,
"raise":0
},
{
"charID":112,
"x":950,
"y":758,
"width":41,
"height":55,
"descender":16,
"raise":0
},
{
"charID":113,
"x":32,
"y":860,
"width":41,
"height":55,
"descender":16,
"raise":0
},
{
"charID":114,
"x":135,
"y":857,
"width":40,
"height":42,
"descender":0,
"raise":0
},
{
"charID":115,
"x":236,
"y":855,
"width":44,
"height":45,
"descender":1,
"raise":0
},
{
"charID":116,
"x":341,
"y":825,
"width":35,
"height":75,
"descender":1,
"raise":0
},
{
"charID":117,
"x":439,
"y":861,
"width":43,
"height":38,
"descender":0,
"raise":0
},
{
"charID":118,
"x":541,
"y":863,
"width":43,
"height":36,
"descender":0,
"raise":0
},
{
"charID":119,
"x":643,
"y":863,
"width":43,
"height":37,
"descender":1,
"raise":0
},
{
"charID":120,
"x":745,
"y":862,
"width":45,
"height":37,
"descender":0,
"raise":0
},
{
"charID":121,
"x":847,
"y":861,
"width":42,
"height":57,
"descender":19,
"raise":0
},
{
"charID":122,
"x":948,
"y":859,
"width":46,
"height":40,
"descender":0,
"raise":0
},
{
"charID":123,
"x":37,
"y":922,
"width":30,
"height":80,
"descender":1,
"raise":0
},
{
"charID":124,
"x":150,
"y":924,
"width":10,
"height":77,
"descender":0,
"raise":0
},
{
"charID":125,
"x":240,
"y":922,
"width":30,
"height":80,
"descender":1,
"raise":0
},
{
"charID":126,
"x":335,
"y":958,
"width":45,
"height":23,
"descender":0,
"raise":20
}
]}
