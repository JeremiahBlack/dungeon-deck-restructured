#ifndef MESSAGING_DISTRIBUTORS_BASE_MESSAGE_DISTRIBUTOR_H
#define MESSAGING_DISTRIBUTORS_BASE_MESSAGE_DISTRIBUTOR_H

#include "Messaging\Distributors\IMessageDistributor.h"

#include <map>
#include <vector>

class BaseMessageDistributor : public IMessageDistributor
{
public:
	//IMessageDistributor interface.

	virtual void DistributeMessage(std::shared_ptr<IMessage> message) override;

	virtual std::unique_ptr<IMessageListener> CreateListener() override;

	virtual void SubscribeListener(IMessageListener* listener, MessageType type) override;

	virtual void UnsubscribeListener(IMessageListener* listener, MessageType type) override;

private:
	std::map<MessageType, std::vector<IMessageListener*> > subscribers;
};

#endif