#ifndef MESSAGING_DISTRIBUTORS_I_MESSAGE_DISTRIBUTOR_H
#define MESSAGING_DISTRIBUTORS_I_MESSAGE_DISTRIBUTOR_H

#include "Messaging\IMessage.h"

#include <memory>

#include "Messaging\Listeners\IMessageListener.h"

class IMessageDistributor
{
public:
	virtual void DistributeMessage(std::shared_ptr<IMessage> message) = 0;
	virtual std::unique_ptr<IMessageListener> CreateListener() = 0;
	virtual void SubscribeListener(IMessageListener* listener, MessageType type) = 0;
	virtual void UnsubscribeListener(IMessageListener* listener, MessageType type) = 0;
};

#endif