#ifndef MESSAGING_OS_MESSAGES_SDL_KEY_UP_MESSAGE_H
#define MESSAGING_OS_MESSAGES_SDL_KEY_UP_MESSAGE_H

#include "Messaging\IMessage.h"

#include <SDL.h>

class SDLKeyUpMessage : public IMessage
{
public:
	// IMessage interface

	static const MessageType Type = 0x21d160b6;

	virtual MessageType GetType() override { return SDLKeyUpMessage::Type; }

	// SDLKeyUpMessage specific

	SDLKeyUpMessage(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) :
		keyCode(keyCode),
		keyMod(keyMod),
		scanCode(scanCode)
	{
		
	}

	inline SDL_Keycode GetKeyCode() { return keyCode; }
	inline SDL_Keymod GetKeyMod() { return keyMod; }
	inline SDL_Scancode GetScanCode() { return scanCode; }
private:
	SDL_Keycode keyCode;
	SDL_Keymod keyMod;
	SDL_Scancode scanCode;
};

#endif