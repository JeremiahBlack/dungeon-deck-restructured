#ifndef GRAPHICS_MESSAGING_OS_MESSAGES_SDL_OS_MESSAGE_HANDLER_H
#define GRAPHICS_MESSAGING_OS_MESSAGES_SDL_OS_MESSAGE_HANDLER_H

#include "Messaging\OS Messages\IOSMessageHandler.h"

#include <SDL.h>

class SDLOSMessageHandler : public IOSMessageHandler
{
public:
	virtual void RunOSMessageLoop(std::shared_ptr<IMessageDistributor> distributor) override;

private:
	SDL_Event event; // So we don't have to recreate an event EVERY time.
};

#endif