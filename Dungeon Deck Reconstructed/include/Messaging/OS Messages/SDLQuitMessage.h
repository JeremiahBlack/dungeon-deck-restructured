#ifndef MESSAGING_OS_MESSAGES_SDL_QUIT_MESSAGE_H
#define MESSAGING_OS_MESSAGES_SDL_QUIT_MESSAGE_H

#include "Messaging\IMessage.h"

class SDLQuitMessage : public IMessage
{
public:
	//IMessage interface

	static const MessageType Type = 0x33099dd2;

	virtual MessageType GetType() override { return SDLQuitMessage::Type; }

};

#endif