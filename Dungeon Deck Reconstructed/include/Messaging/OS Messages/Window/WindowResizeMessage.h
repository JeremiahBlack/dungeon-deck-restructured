#ifndef MESSAGING_OS_MESSAGES_WINDOW_WINDOW_RESIZE_MESSAGE_H
#define MESSAGING_OS_MESSAGES_WINDOW_WINDOW_RESIZE_MESSAGE_H

#include "Messaging\IMessage.h"

#include "Utilities\Point.h"

class WindowResizeMessage : public IMessage
{
public:
	const static MessageType Type = 0xcd11ea90;
	virtual MessageType GetType() override { return WindowResizeMessage::Type; }
	WindowResizeMessage(Point<int> size) : newSize(size) {}
	WindowResizeMessage(int x, int y) : newSize(x, y) {}
	inline const Point<int>& GetSize() const { return newSize; }
private:
	Point<int> newSize;
};

#endif