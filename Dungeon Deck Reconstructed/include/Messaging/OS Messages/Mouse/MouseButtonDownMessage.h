#ifndef MESSAGING_OS_MESSAGES_MOUSE_BUTTON_DOWN_MESSAGE_H
#define MESSAGING_OS_MESSAGES_MOUSE_BUTTON_DOWN_MESSAGE_H

#include "Messaging\IMessage.h"
#include "Messaging\OS Messages\Mouse\MouseButtonType.h"

class MouseButtonDownMessage : public IMessage
{
public:
	const static MessageType Type = 0xef5633a;
	virtual MessageType GetType() override { return MouseButtonDownMessage::Type; }

	MouseButtonDownMessage(MouseButton button) : buttonPressed(button) {}
	const MouseButton& GetButton() const{ return buttonPressed; }
private:
	MouseButton buttonPressed;
};

#endif