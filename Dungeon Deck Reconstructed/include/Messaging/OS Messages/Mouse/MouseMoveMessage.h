#ifndef MESSAGING_OS_MESSAGES_MOUSE_MOVE_MESSAGE_H
#define MESSAGING_OS_MESSAGES_MOUSE_MOVE_MESSAGE_H

#include "Messaging\IMessage.h"

#include "Utilities\Point.h"

class MouseMoveMessage : public IMessage
{
public:
	const static MessageType Type = 0x66b5f77b;
	virtual MessageType GetType() override { return MouseMoveMessage::Type; }

	MouseMoveMessage(Point<int> location) : location(location) {}
	MouseMoveMessage(int x, int y) : location(x, y) {}
	inline const Point<int>& GetLocation() const { return location; }
private:
	Point<int> location;
};

#endif