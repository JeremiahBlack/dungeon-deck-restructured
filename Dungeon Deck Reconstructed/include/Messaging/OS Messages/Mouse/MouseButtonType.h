#ifndef MESSAGING_OS_MESSAGES_MOUSE_BUTTON_TYPE_H
#define MESSAGING_OS_MESSAGES_MOUSE_BUTTON_TYPE_H

enum MouseButton { Left, Right, Middle, None};

#endif