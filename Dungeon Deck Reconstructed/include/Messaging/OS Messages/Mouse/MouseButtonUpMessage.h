#ifndef MESSAGING_OS_MESSAGES_MOUSE_BUTTON_UP_MESSAGE_H
#define MESSAGING_OS_MESSAGES_MOUSE_BUTTON_UP_MESSAGE_H

#include "Messaging\IMessage.h"
#include "Messaging\OS Messages\Mouse\MouseButtonType.h"

class MouseButtonUpMessage : public IMessage
{
public:
	const static MessageType Type = 0x1e547728;
	virtual MessageType GetType() override { return MouseButtonUpMessage::Type; }

	MouseButtonUpMessage(MouseButton button) : buttonReleased(button) {}
	const MouseButton& GetButton() const{ return buttonReleased; }
private:
	MouseButton buttonReleased;
};

#endif