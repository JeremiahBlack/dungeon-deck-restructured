#ifndef MESSAGING_OS_MESSAGES_MOUSE_WHEEL_SCROLL_MESSAGE_H
#define MESSAGING_OS_MESSAGES_MOUSE_WHEEL_SCROLL_MESSAGE_H

#include "Messaging\IMessage.h"

class MouseWheelScrollMessage : public IMessage
{
public:
	const static MessageType Type = 0x5e0afce1;
	virtual MessageType GetType() override { return MouseWheelScrollMessage::Type; }

	MouseWheelScrollMessage(int x, int y) : xScroll(x), yScroll(y) {}
	const int GetVerticalScroll() { return xScroll; }
	const int GetHorizontalScroll() { return yScroll; }
private:
	int xScroll;
	int yScroll;
};

#endif