#ifndef MESSAGING_OS_MESSAGES_I_OS_MESSAGE_HANDLER_H
#define MESSAGING_OS_MESSAGES_I_OS_MESSAGE_HANDLER_H

#include "Messaging\Distributors\IMessageDistributor.h"

#include <memory>

class IOSMessageHandler
{
public:
	// Run through the current OS message queue.
	virtual void RunOSMessageLoop(std::shared_ptr<IMessageDistributor> distributor) = 0;
};

#endif