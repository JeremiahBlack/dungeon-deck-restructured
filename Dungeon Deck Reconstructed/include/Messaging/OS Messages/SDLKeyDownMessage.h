#ifndef MESSAGING_OS_MESSAGES_SDL_KEY_DOWN_MESSAGE_H
#define MESSAGING_OS_MESSAGES_SDL_KEY_DOWN_MESSAGE_H

#include "Messaging\IMessage.h"

#include <SDL.h>

class SDLKeyDownMessage : public IMessage
{
public:
	// IMessage interface
	
	static const MessageType Type = 0xf692cd3a;
	
	virtual MessageType GetType() override { return SDLKeyDownMessage::Type; }

	// SDLKeyDownMessage specific
	
	SDLKeyDownMessage(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode);

	inline SDL_Keycode GetKeyCode() { return keyCode; }
	inline SDL_Keymod GetKeyMod() { return keyMod; }
	inline SDL_Scancode GetScanCode() { return scanCode; }
private:
	SDL_Keycode keyCode;
	SDL_Keymod keyMod;
	SDL_Scancode scanCode;
};

#endif