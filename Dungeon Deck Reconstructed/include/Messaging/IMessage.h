#ifndef MESSAGING_I_MESSAGE_H
#define MESSAGING_I_MESSAGE_H

typedef unsigned int MessageType;

class IMessage
{
public:
	// Retrieve the message type.
	virtual MessageType GetType() = 0;
};

#endif