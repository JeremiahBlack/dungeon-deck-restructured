#ifndef MESSAGING_LISTENERS_I_MESSAGE_LISTENER_H
#define MESSAGING_LISTENERS_I_MESSAGE_LISTENER_H

#include "Messaging\IMessage.h"

#include <memory>

class IMessageListener
{
public:
	// Add a type of message for the listener to listen for.
	virtual void AddSubscription(MessageType type) = 0;

	// Remove a type of message the listener is listening for.
	virtual void RemoveSubscription(MessageType type) = 0;

	// Receive a message to store until polled.
	virtual void ReceiveMessage(std::shared_ptr<IMessage> message) = 0;

	// Grab the next Message.
	virtual std::shared_ptr<IMessage> PollMessage() = 0;
};

#endif