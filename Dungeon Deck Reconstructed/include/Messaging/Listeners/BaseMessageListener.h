#ifndef MESSAGING_LISTENERS_BASE_MESSAGE_LISTENER_H
#define MESSAGING_LISTENERS_BASE_MESSAGE_LISTENER_H

#include "Messaging\Listeners\IMessageListener.h"

#include <vector>
#include <queue>

#include "Messaging\Distributors\IMessageDistributor.h"

class BaseMessageListener : public IMessageListener
{
public:
	// IMessageListener interface.

	virtual void AddSubscription(MessageType type) override;

	virtual void RemoveSubscription(MessageType type) override;

	virtual void ReceiveMessage(std::shared_ptr<IMessage> message) override;

	virtual std::shared_ptr<IMessage> PollMessage() override;

	// BaseMessageListener specific.

	BaseMessageListener(IMessageDistributor* parentDistributor);
	virtual ~BaseMessageListener();

private:
	IMessageDistributor* parentDistributor; // This shouldn't be controlled here, as it's the parent.
	std::vector<MessageType> subscriptions;
	std::queue<std::shared_ptr<IMessage>> messageQueue;
};

#endif