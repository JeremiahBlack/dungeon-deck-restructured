#ifndef GRAPHICS_FACTORY_SDL_GRAPHIC_RESOURCE_FACTORY_H
#define GRAPHICS_FACTORY_SDL_GRAPHIC_RESOURCE_FACTORY_H

#include "Graphics\Factory\IGraphicResourceFactory.h"

#include <SDL.h>
#include <map>

#include "Graphics\Text\FontCharacterInfo.h"
#include "Graphics\Sprite\SpriteInfo.h"
#include "Messaging\OS Messages\Window\WindowResizeMessage.h"

class SDLGraphicResourceFactory : public IGraphicResourceFactory
{
public:
	// IGraphicResourceFactory interface.

	virtual bool Initalize(std::shared_ptr<IMessageDistributor> osMessageDistributor) override;

	virtual void Destroy() override;

	virtual void BeginRender() override
	{
		while (message = osMessageListener->PollMessage())
		{
			std::shared_ptr<WindowResizeMessage> castMessage = std::dynamic_pointer_cast<WindowResizeMessage>(message);
			windowWidth = castMessage->GetSize().x;
			windowHeight = castMessage->GetSize().y;
			xScale = static_cast<float>(windowWidth) / 1920.0f;
			yScale = static_cast<float>(windowHeight) / 1080.0f;
		}
		SDL_RenderClear(mainRenderer);
	}

	virtual void EndRender() override
	{
		SDL_RenderPresent(mainRenderer);
	}

	virtual Sprite CreateSprite
		(std::string name, int xLocation, int yLocation, int width, int height) override;

	virtual void RenderSprite(int ID, Transform transform) override;

	// SDLGraphicResourceFactory specific.

	SDLGraphicResourceFactory();
	virtual ~SDLGraphicResourceFactory();

private:
	int windowWidth, windowHeight; // Window information.
	float xScale, yScale; // The scaling required for aspect ratio.
	SDL_Window* mainWindow; // The window handle for the game. It lasts the lifetime of the factory.
	SDL_Renderer* mainRenderer; // The rendering context for the game. It lasts the lifetime of the factory.
	SDL_Texture* fontTexture; // This is the texture used for rendering font. It lasts the lifetime of the factory.
	std::map<char, FontCharacterInfo> fontCharBounds; // Char is the character for the FontCharacterInfo.
	int newSpriteID; // This is the ID that can be used for the next created sprite.
	std::map<int, SpriteInfo> sprites; // Int is the ID correlated to the SpriteInfo.
	std::map<std::string, SDL_Texture*> textureMap; // The string is the filename of the loaded texture.
	std::unique_ptr<IMessageListener> osMessageListener;
	std::shared_ptr<IMessage> message;
};

#endif