#ifndef GRAPHICS_FACTORY_I_GRAPHIC_RESOURCE_FACTORY_H
#define GRAPHICS_FACTORY_I_GRAPHIC_RESOURCE_FACTORY_H

/*
Purpose : An abstracted class that allows clients to make sprite resources.
*/

#include <memory>
#include <string>

#include "Graphics\Sprite\Sprite.h"
#include "Messaging\Distributors\IMessageDistributor.h"

class IGraphicResourceFactory
{
public:
	// Initalizes any required resources.
	virtual bool Initalize(std::shared_ptr<IMessageDistributor> osMessageDistributor) = 0;

	// Release any resources being used
	virtual void Destroy() = 0;

	virtual void BeginRender() = 0;
	virtual void EndRender() = 0;

	// Create a sprite for the client.
	virtual Sprite CreateSprite(std::string Filename, int sheetXLocation, int sheetYLocation, int spriteWidth, int spriteHeight) = 0;

	virtual void RenderSprite(int ID, Transform transform) = 0;
};

#endif