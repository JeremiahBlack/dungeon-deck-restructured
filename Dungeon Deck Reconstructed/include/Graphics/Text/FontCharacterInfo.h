#ifndef GRAPHICS_TEXT_FONT_CHARACTER_INFO_H
#define GRAPHICS_TEXT_FONT_CHARACTER_INFO_H

struct FontCharacterInfo
{
	unsigned int x; // X location on the texture.
	unsigned int y; // Y location on the texture.
	unsigned int width; // The width of the character.
	unsigned int height; // The height of the character.
	unsigned int descender; // How far below the character goes underneath the base line. j & q are characters that do so.
	unsigned int raise; // How high above the base line a character is.
};

#endif