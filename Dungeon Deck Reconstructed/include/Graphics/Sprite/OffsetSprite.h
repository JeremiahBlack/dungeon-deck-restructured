#ifndef GRAPHICS_SPRITE_OFFSET_SPRITE_H
#define GRAPHICS_SPRITE_OFFSET_SPRITE_H

#include "Graphics\Sprite\ISprite.h"

#include <memory>

class OffsetSprite : public ISprite
{
public:

	// ISprite interface

	virtual void ChangeDisplaySize(int width, int height) { primarySprite->ChangeDisplaySize(width, height); }

	virtual void Render(int xLocation, int yLocation) 
	{ primarySprite->Render(xLocation + xOffset, yLocation + yOffset); }

	// OffsetSprite specific.

	OffsetSprite(std::shared_ptr<ISprite> mainSprite, int xOffset, int yOffset) :
		primarySprite(mainSprite),
		xOffset(xOffset),
		yOffset(yOffset)
	{

	}

private:
	std::shared_ptr<ISprite> primarySprite;
	int xOffset, yOffset;
};

#endif