#ifndef GRAPHICS_SPRITE_SDL_STATIC_SPRITE_H
#define GRAPHICS_SPRITE_SDL_STATIC_SPRITE_H

#include "Graphics\Sprite\ISprite.h"

#include <SDL.h>

class SDLStaticSprite : public ISprite
{
public:
	// ISprite interface.

	virtual void ChangeDisplaySize(int width, int height) override;

	virtual void Render(int xLocation, int yLocation) override;

	// SDLStaticSprite specific.
	SDLStaticSprite(SDL_Renderer* targetRenderer, SDL_Texture* texture, SDL_Rect imageDimensions);

protected:
	SDL_Texture* texture;

private:
	SDL_Renderer* targetRenderer;
	SDL_Rect imageDimensions;
	SDL_Rect locationToDraw;
};

#endif