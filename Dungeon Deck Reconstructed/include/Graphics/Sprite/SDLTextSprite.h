#ifndef GRAPHICS_SPRITE_SDL_TEXT_SPRITE_H
#define GRAPHICS_SPRITE_SDL_TEXT_SPRITE_H

#include "Graphics\Sprite\SDLStaticSprite.h"

class SDLTextSprite : public SDLStaticSprite
{
public:
	// SDLTextSprite specific.

	SDLTextSprite(SDL_Renderer* targetRenderer, SDL_Texture* texture, SDL_Rect imageDimensions)
		: SDLStaticSprite(targetRenderer, texture, imageDimensions) {}
	virtual ~SDLTextSprite();

private:

};

#endif