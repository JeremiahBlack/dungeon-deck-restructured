#ifndef GRAPHICS_SPRITE_SDL_BITMAP_TEXT_SPRITE_H
#define GRAPHICS_SPRITE_SDL_BITMAP_TEXT_SPRITE_H

#include "Graphics\Sprite\ISprite.h"

#include <string>
#include <vector>

#include <SDL.h>

class SDLBitmapTextSprite : public ISprite
{
public:
	virtual void ChangeDisplaySize(int width, int height) override {}

	virtual void Render(int xLocation, int yLocation);

	SDLBitmapTextSprite(SDL_Texture* texture, SDL_Renderer* renderer, std::vector<std::pair<SDL_Rect, SDL_Rect> > listOfBounds);
private:
	SDL_Texture* texture;
	SDL_Renderer* renderer;
	std::vector<std::pair<SDL_Rect, SDL_Rect> > letterBounds;
};

#endif