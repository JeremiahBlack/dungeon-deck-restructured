/*
Purpose: Act as an interface to the rest of the software for a graphics object.
Issues:
	- This is going to need a reference to what it has to change (right now, the GraphicsFactory).
*/

#ifndef GRAPHICS_SPRITE_SPRITE_H
#define GRAPHICS_SPRITE_SPRITE_H

#include "Utilities\Transform.h"
class IGraphicResourceFactory;

class Sprite
{
public:
	Sprite();
	Sprite(int refID, IGraphicResourceFactory* parent);
	~Sprite(){}
	Sprite(const Sprite& copy);

	void Render(Transform parentTransform = Transform::Identity());
private:
	Transform localTransform;
	int referenceID;

	IGraphicResourceFactory* parent;
};

#endif