#ifndef GRAPHICS_SPRITE_SPRITE_INFO_H
#define GRAPHICS_SPRITE_SPRITE_INFO_H

#include <SDL.h>

struct SpriteInfo
{
	int x, y;
	unsigned int width, height;
	bool active;
	SDL_Texture* texture;
};

#endif