#ifndef GRAPHICS_SPRITE_I_SPRITE_H
#define GRAPHICS_SPRITE_I_SPRITE_H

#include <string>

class ISprite
{
public:
	// Change sprite display size.
	virtual void ChangeDisplaySize(int width, int height) = 0;

	// Draw at specified pixel location. Assumed to be the top right of the location.
	virtual void Render(int xLocation, int yLocation) = 0;
};



#endif