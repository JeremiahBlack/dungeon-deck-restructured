#ifndef CREATURES_ATTRIBUTE_H
#define CREATURES_ATTRIBUTE_H

class Attribute
{
public:
	Attribute(unsigned int value);

	void DamageAttribute(unsigned int value);
	void HealAttribute(unsigned int value);

	inline const unsigned int GetCurrentValue() const { return attributeValue; }
	inline const unsigned int GetMaxValue() const { return attributeMaxValue; }

	void IncreaseMaxValue(unsigned int increaseValue);

private:
	unsigned int attributeValue;
	unsigned int attributeMaxValue;
};

#endif