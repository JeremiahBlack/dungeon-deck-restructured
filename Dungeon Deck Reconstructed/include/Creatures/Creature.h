#ifndef CREATURES_CREATURE_H
#define CREATURES_CREATURE_H

#include "Creatures\Attribute.h"

#include <string>

class Creature
{
public:
	Creature(std::string name, unsigned int health = 5,
		unsigned int defense = 1, unsigned int attack = 1);

	// Check to see if the creature is dead (no longer able to fight). Returns true if they are dead.
	bool IsDead();

	// Get the creature's given name.
	inline const std::string GetName() const { return name; }

	inline const Attribute GetHealth() const { return health; }
	inline const Attribute GetDefense() const { return defense; }
	inline const Attribute GetAttack() const { return attack; }

	// Attack the given creature.
	void Attack(Creature& target);

private:
	std::string name;
	Attribute health, defense, attack;
};

#endif