#ifndef CARDS_CARD_H
#define CARDS_CARD_H

#include <memory>

#include "Creatures\Creature.h"
#include "Graphics\Sprite\ISprite.h"

class Card
{
public:
	Card(std::shared_ptr<ISprite> cardSprite, int xPosition, int yPosition, Creature creature) :
		cardSprite(cardSprite), xPosition(xPosition), yPosition(yPosition), creature(creature)
	{

	}

	// Draw the card.
	void Render()
	{
		cardSprite->Render(xPosition, yPosition);
	}

	inline Creature GetCreature() { return creature; }

private:
	std::shared_ptr<ISprite> cardSprite;
	int xPosition, yPosition;
	Creature creature;
};

#endif