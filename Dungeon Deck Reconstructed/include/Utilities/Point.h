#ifndef UTILITIES_POINT_H
#define UTILITIES_POINT_H

template <typename T>
struct Point
{
	T x, y;
	Point() : x(0), y(0) {}
	Point(T x, T y) : x(x), y(y) {}
};

#endif