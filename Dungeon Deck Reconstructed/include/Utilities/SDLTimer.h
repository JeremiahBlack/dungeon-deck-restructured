#ifndef UTILITIES_SDL_TIMER_H
#define UTILITIES_SDL_TIMER_H

#include "Utilities\ITimer.h"

#include <SDL.h>

class SDLTimer : public ITimer
{
public:

	// ITimer interface

	virtual void Start() override;
	virtual void Stop() override;
	virtual void Pause() override;
	virtual void Resume() override;

	virtual unsigned int GetCurrentMilliseconds() override;

	// SDLTimer specific

	SDLTimer() : startTime(0), pausedTime(0), isPaused(true) {}

private:
	Uint32 startTime;
	Uint32 pausedTime;
	bool isPaused;
};

#endif