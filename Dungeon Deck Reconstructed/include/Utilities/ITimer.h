#ifndef UTILITIES_I_TIMER_H
#define UTILITIES_I_TIMER_H

class ITimer
{
public:
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual unsigned int GetCurrentMilliseconds() = 0;
};

#endif