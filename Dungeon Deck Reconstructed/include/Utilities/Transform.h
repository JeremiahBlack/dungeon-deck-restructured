#ifndef UTILITIIES_TRANSFORM_H
#define UTILITIIES_TRANSFORM_H

#include "Utilities\Point.h"

struct Transform
{
	Point<float> location;
	float rotation; // In degrees, not radians.
	Point<float> scale;

	static Transform Identity()
	{
		Transform tempTransform = { { 0.0f, 0.0f }, 0.0f, { 1.0f, 1.0f } };
		return tempTransform;
	}

	const Transform& Combine(Transform& other) // This overrides the current Tranform. Might not be the behaviour I want.
	{
		location.x += other.location.x;
		location.y += other.location.y;
		rotation += other.rotation;
		scale.x *= other.scale.x;
		scale.y *= other.scale.y;
		return *this;
	}
};

#endif