#ifndef LOGIC_GAME_OBJECTS_COMPONENTS_GAME_COMPONENT_H
#define LOGIC_GAME_OBJECTS_COMPONENTS_GAME_COMPONENT_H

#include "Utilities\Transform.h"

typedef unsigned int GameComponentID;

enum ComponentType { Graphical, Audio, Logic };

class GameComponent
{
public:
	GameComponent(GameComponentID ID, Transform& ParentTransform) : id(ID), parentTransform(ParentTransform) {}
	const GameComponent& operator=(const GameComponent& copy) { if (this == &copy) return *this; id = copy.id; parentTransform = copy.parentTransform; return *this; }
	virtual ~GameComponent() {}
	virtual ComponentType GetComponentType() {	return ComponentType::Logic; }
	virtual bool Initialize() = 0;
	virtual void Destroy() = 0;
	virtual void Update() = 0;
protected:
	GameComponentID id;
	Transform& parentTransform;
};

#endif