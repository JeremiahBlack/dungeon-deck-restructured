#ifndef LOGIC_GAME_OBJECTS_COMPONENTS_RENDERABLE_COMPONENT_H
#define LOGIC_GAME_OBJECTS_COMPONENTS_RENDERABLE_COMPONENT_H

#include "Logic\Game Objects\Components\GameComponent.h"

class RenderableComponent : public GameComponent
{
public:
	RenderableComponent(GameComponentID ID, Transform& ParentTransform) : GameComponent(ID, ParentTransform) {}
	virtual ComponentType GetComponentType() override { return ComponentType::Graphical; }
	virtual void Render() = 0;
};

#endif