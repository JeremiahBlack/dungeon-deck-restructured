#ifndef LOGIC_GAME_OBJECTS_COMPONENTS_AUDIO_COMPONENT_H
#define LOGIC_GAME_OBJECTS_COMPONENTS_AUDIO_COMPONENT_H

#include "Logic\Game Objects\Components\GameComponent.h"

class AudioComponent : public GameComponent
{
public:
	AudioComponent(GameComponentID ID, Transform& ParentTransform) : GameComponent(ID, ParentTransform) {}
	virtual ComponentType GetComponentType() override { return ComponentType::Audio; }

	virtual void Play() = 0;
	virtual void Stop() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;
};

#endif