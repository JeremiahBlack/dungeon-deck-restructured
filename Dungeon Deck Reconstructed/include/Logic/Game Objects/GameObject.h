#ifndef LOGIC_GAME_OBJECTS_GAME_OBJECT_H
#define LOGIC_GAME_OBJECTS_GAME_OBJECT_H

#include "Utilities\Transform.h"
#include "Logic\Game Objects\Components\GameComponent.h"
#include "Logic\Game Objects\Components\RenderableComponent.h"
#include <vector>

typedef unsigned int GameObjectID;

class GameObject
{
public:
	GameObject(GameObjectID ID, GameObject* Parent = nullptr, Transform LocalTransform = Transform::Identity()) :
		id(ID), localTransform(LocalTransform), parent(Parent) {}

	Transform localTransform;
private:
	GameObject* parent;
	GameObjectID id;
	std::vector<GameComponent*> updateComponents;
	std::vector<RenderableComponent*> renderComponents;
};

#endif