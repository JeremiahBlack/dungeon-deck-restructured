#ifndef LOGIC_GAME_OBJECTS_GAME_OBJECT_FACTORY_H
#define LOGIC_GAME_OBJECTS_GAME_OBJECT_FACTORY_H

#include "Graphics\Factory\IGraphicResourceFactory.h"
#include "Logic\Game Objects\GameObject.h"
#include "json\json.h"

class GameObjectFactory
{
public:
	GameObjectFactory(IGraphicResourceFactory* GraphicsFactory) : currentID(1), graphicsFactory(GraphicsFactory) {}
	~GameObjectFactory(){}

	GameObject* CreateNewGameObject(Json::Value information);
private:
	GameObjectID currentID;
	IGraphicResourceFactory* graphicsFactory;
};

#endif