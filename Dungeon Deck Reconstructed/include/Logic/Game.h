#ifndef LOGIC_GAME_H
#define LOGIC_GAME_H

#include "Graphics\Factory\IGraphicResourceFactory.h"
#include "Messaging\Distributors\IMessageDistributor.h"
#include "Logic\Game States\GameState.h"

class Game
{
public:

	Game(std::shared_ptr<IGraphicResourceFactory> GraphicsFactory, std::shared_ptr<IMessageDistributor> OSMessageDistributor);

	~Game();

	bool Initialize();

	void Destroy();

	// Update the game logic by a frame.
	void Update();

	// Render the game.
	void Render();

	bool IsRunning() { return isRunning; }

	void Quit() { isRunning = false; }

private:
	bool isRunning; // Ensure the game is running.

	std::shared_ptr<IGraphicResourceFactory> graphicsFactory;
	std::shared_ptr<IMessageDistributor> osMessageDistributor;
	GameState* currentState;
};

#endif