#ifndef LOGIC_NEW_GRAPHICS_SYSTEM_GAME_H
#define LOGIC_NEW_GRAPHICS_SYSTEM_GAME_H

#include <memory>

#include "Graphics\Factory\IGraphicResourceFactory.h"
#include "Messaging\Distributors\IMessageDistributor.h"

class NewGraphicsSystemGame
{
public:

	NewGraphicsSystemGame(std::shared_ptr<IGraphicResourceFactory> graphicsFactory, std::shared_ptr<IMessageDistributor> osMessageDistributor);


	// Update the game logic by a frame.
	void Update();

	// Render the game.
	void Render();

	bool IsRunning() { return isRunning; }

	void Quit() { isRunning = false; }

private:
	bool isRunning; // Ensure the game is running.
	// State control.
	std::unique_ptr<IMessageListener> osMessageListener;
	std::shared_ptr<IMessage> message;
	Sprite testSprite;
};

#endif