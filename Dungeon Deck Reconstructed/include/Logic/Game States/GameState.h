#ifndef LOGIC_GAME_STATES_GAME_STATE_H
#define LOGIC_GAME_STATES_GAME_STATE_H

#include <memory>

#include "Graphics\Factory\IGraphicResourceFactory.h"
#include "Messaging\Distributors\IMessageDistributor.h"

class GameState
{
public:
	GameState(std::shared_ptr<IGraphicResourceFactory> GraphicFactory, std::shared_ptr<IMessageDistributor> OSMessageDistributor) :
		graphicFactory(GraphicFactory)
	{
		messageListener = OSMessageDistributor->CreateListener();
	}

	virtual bool Initialize() 
	{
		return true;
	}
	virtual void Destroy() {}

	virtual void Pause() {}
	virtual void Resume() {}

	virtual void Update() {}
	virtual void Render() {}
protected:
	std::shared_ptr<IGraphicResourceFactory> graphicFactory;
	std::shared_ptr<IMessageListener> messageListener;
};

#endif