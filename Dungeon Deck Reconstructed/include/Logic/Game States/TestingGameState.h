#ifndef LOGIC_GAME_STATES_TESTING_GAME_STATE_H
#define LOGIC_GAME_STATES_TESTING_GAME_STATE_H

#include "Logic\Game States\GameState.h"

class TestingGameState : public GameState
{
public:
	TestingGameState(std::shared_ptr<IGraphicResourceFactory> GraphicFactory, std::shared_ptr<IMessageDistributor> OSMessageDistributor);
	
	virtual bool Initialize() override;
	virtual void Destroy() override;
	
	virtual void Pause() override;
	virtual void Resume() override;

	virtual void Update() override;
	virtual void Render() override;
private:
	Sprite background;
};

#endif