#include <memory>

#include <SDL.h>
#include <SDL_image.h>

#include "Graphics\Factory\SDLGraphicResourceFactory.h"

#include "Logging\logger.h"

#include "Logic\Game.h"

// Setting up a messaging system.
#include "Messaging\Distributors\BaseMessageDistributor.h"
#include "Messaging\OS Messages\SDLOSMessageHandler.h"
#include "Messaging\OS Messages\SDLQuitMessage.h"

#include "Utilities\SDLTimer.h"

// main function format required to use SDL.
int main(int, char**)
{
	// Initialize SDL and its other systems
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		// SDL Failed to initialize!
		LOG_ERR("SDL failed to initailize: ", SDL_GetError());
		return 1;
	}
		
	if (IMG_Init(IMG_InitFlags::IMG_INIT_PNG) != IMG_InitFlags::IMG_INIT_PNG)
	{
		// SDL_image failed to initialize!
		LOG_ERR("SDL_image failed to initialize: ", IMG_GetError());
		return 1;
	} 

	// Time to make a messaging system.
	std::shared_ptr<IMessageDistributor> messageDistributor = std::make_shared<BaseMessageDistributor>();
	std::unique_ptr<IMessageListener> listener = messageDistributor->CreateListener();
	std::unique_ptr<IOSMessageHandler> osMessageHandler = std::make_unique<SDLOSMessageHandler>();

	listener->AddSubscription(SDLQuitMessage::Type);

	std::unique_ptr<ITimer> timer = std::make_unique<SDLTimer>();

	// Create the graphics factory!
	std::shared_ptr<IGraphicResourceFactory> graphicFactory = std::make_shared<SDLGraphicResourceFactory>();
	if (!graphicFactory->Initalize(messageDistributor))
	{
		// Graphics factory failed to initialize!
		LOG_ERR("Graphics factory failed to initialize!");
		return 1;
	}

	Game game(graphicFactory, messageDistributor);

	if (!game.Initialize())
	{
		LOG_ERR("Game failed to initialize!");
		return 1;
	}

	// Preparing for game loop.
	// Timer stuff.
	timer->Start();
	unsigned int lastUpdateFinishTime = 0;
	unsigned int millsecondsPerFrame = 1000 / 60;
	// Our message so we don't recreate it every loop.
	std::shared_ptr<IMessage> message;
	while (game.IsRunning())
	{
		osMessageHandler->RunOSMessageLoop(messageDistributor);
		while (message = listener->PollMessage())
		{
			if (message->GetType() == SDLQuitMessage::Type)
			{
				game.Quit();
			}
		}

		if (timer->GetCurrentMilliseconds() - lastUpdateFinishTime >= millsecondsPerFrame)
		{
			game.Update();
			graphicFactory->BeginRender();
			game.Render();
			graphicFactory->EndRender();

			lastUpdateFinishTime = timer->GetCurrentMilliseconds();
		}
	}

	game.Destroy();

	// Release the messaging system stuff.
	osMessageHandler.release();
	listener.release();
	messageDistributor = nullptr;

	graphicFactory->Destroy();
	graphicFactory = nullptr;

	// Close down SDL and its other systems.
	IMG_Quit();
	SDL_Quit();

	return 0;
}