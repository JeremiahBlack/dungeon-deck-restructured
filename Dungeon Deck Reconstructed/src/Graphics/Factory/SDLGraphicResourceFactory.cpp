#include "Graphics\Factory\SDLGraphicResourceFactory.h"

#include <fstream>

#include <json\json.h>
#include <SDL_image.h>

#include "Graphics\Sprite\SpriteInfo.h"

#include "Logging\logger.h"

bool SDLGraphicResourceFactory::Initalize(std::shared_ptr<IMessageDistributor> osMessageDistributor)
{
	osMessageListener = osMessageDistributor->CreateListener();
	osMessageListener->AddSubscription(WindowResizeMessage::Type);
	newSpriteID = 0;
	// Load window configuration.
	std::ifstream configFile("Configs//Window.js");
	if (!configFile.is_open())
	{
		LOG_ERR("Failed to open window configuration file.");
		return false;
	}
	Json::Value windowConfig;
	configFile >> windowConfig;
	configFile.close();
	windowWidth = windowConfig["Window"].get("Width", 800).asInt();
	windowHeight = windowConfig["Window"].get("Height", 600).asInt();

	xScale = static_cast<float>(windowWidth) / 1920.0f;
	yScale = static_cast<float>(windowHeight) / 1080.0f;

	// Load font configuration.
	std::ifstream fontConfigFile("Configs//Font.js");
	if (!fontConfigFile.is_open())
	{
		LOG_ERR("Failed to open font configuration file.");
		return false;
	}
	Json::Value fontConfig;
	fontConfigFile >> fontConfig;
	fontConfigFile.close();

	if (!fontConfig["Font"].isArray())
	{
		LOG_ERR("Font configuration is not formatted correctly.");
		return false;
	}
	char currentChar = ' ';
	FontCharacterInfo currentCharInfo;
	currentCharInfo.x = 1000;
	currentCharInfo.y = 1000;
	currentCharInfo.height = 24;
	currentCharInfo.width = 24;
	currentCharInfo.descender = 0;
	fontCharBounds.insert(std::pair<char, FontCharacterInfo>(currentChar, currentCharInfo));

	for (auto i = fontConfig["Font"].begin(); i != fontConfig["Font"].end(); i++)
	{
		currentChar = static_cast<char>((*i).get("charID", 0).asInt());
		if (currentChar == 0)
		{
			LOG_ERR("Font configuration has a bad charID.");
			return false;
		}
		currentCharInfo.x = (*i).get("x", 0).asUInt();
		if (currentCharInfo.x == 0)
		{
			LOG_ERR("Font configuration has a bad X location.");
			return false;
		}
		currentCharInfo.y = (*i).get("y", 0).asUInt();
		if (currentCharInfo.y == 0)
		{
			LOG_ERR("Font configuration has a bad Y location.");
			return false;
		}
		currentCharInfo.height = (*i).get("height", 0).asUInt();
		if (currentCharInfo.height == 0)
		{
			LOG_ERR("Font configuration has a bad height.");
			return false;
		}
		currentCharInfo.width = (*i).get("width", 0).asUInt();
		if (currentCharInfo.width == 0)
		{
			LOG_ERR("Font configuration has a bad width.");
			return false;
		}
		currentCharInfo.descender = (*i).get("descender", 0).asUInt();
		currentCharInfo.raise = (*i).get("raise", 0).asUInt();
		fontCharBounds.insert(std::pair<char, FontCharacterInfo>(currentChar, currentCharInfo));
	}

	mainWindow = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (mainWindow == nullptr)
	{
		LOG_ERR("Failed to create a SDL_Window. SDL_Error: ", SDL_GetError());
		return false;
	}

	mainRenderer = SDL_CreateRenderer(mainWindow, -1, SDL_RendererFlags::SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (mainRenderer == nullptr)
	{
		LOG_ERR("Failed to create a SDL_Renderer. SDL_Error: ", SDL_GetError());
		SDL_DestroyWindow(mainWindow);
		return false;
	}

	fontTexture = IMG_LoadTexture(mainRenderer, "Images//Fullsize Bitmap Font.png");
	if (fontTexture == nullptr)
	{
		LOG_ERR("Failed to load Images/Fullsize Bitmap Font.png.");
		SDL_DestroyWindow(mainWindow);
		SDL_DestroyRenderer(mainRenderer);
		return false;
	}
	
	SDL_SetRenderDrawColor(mainRenderer, 0x64, 0x95, 0xed, SDL_ALPHA_OPAQUE);

	return true;
}

void SDLGraphicResourceFactory::Destroy()
{
	for (auto pair : textureMap)
	{
		SDL_DestroyTexture(pair.second);
	}

	textureMap.clear();

	if (fontTexture != nullptr)
	{
		SDL_DestroyTexture(fontTexture);
		fontTexture = nullptr;
	}

	if (mainRenderer != nullptr)
	{
		SDL_DestroyRenderer(mainRenderer);
		mainRenderer = nullptr;
	}

	if (mainWindow != nullptr)
	{
		SDL_DestroyWindow(mainWindow);
		mainWindow = nullptr;
	}
}


Sprite SDLGraphicResourceFactory::CreateSprite
(std::string name, int xLocation, int yLocation, int width, int height)
{
	SDL_Texture* texture = nullptr;
	auto textureMapIterator = textureMap.find(name);
	if (textureMapIterator == textureMap.end()) // Texture wasn't found.
	{
		texture = IMG_LoadTexture(mainRenderer, name.c_str());
		
		if (texture == nullptr)
		{
			// texture load failed. Just abort and send back a null pointer.
			LOG_WARN("SDL_Texture failed to load for creating a sprite. IMG_Error: ", IMG_GetError());
			Sprite tempSprite(-1, this);
			return tempSprite;
		}

		textureMap.insert(std::pair<std::string, SDL_Texture*>(name, texture));
	}
	else // Texture was found!
	{
		texture = textureMapIterator->second;
	}

	// At this point, we have the correct texture. Just need to make the sprite information.
	SpriteInfo spriteInfo = { xLocation, yLocation, width, height, true, texture };
	sprites.insert(std::pair<int, SpriteInfo>(newSpriteID, spriteInfo));
	
	Sprite newSprite(newSpriteID, this);
	++newSpriteID;

	return newSprite;
}


/* - I might need this code still.
std::shared_ptr<ISprite> SDLGraphicResourceFactory::CreateText(std::string text)
{
	std::vector<std::pair<SDL_Rect, SDL_Rect> > renderLocations;
	SDL_Rect textureLocation;
	SDL_Rect whereToRender;
	whereToRender.x = 0;
	whereToRender.y = 0;
	whereToRender.h = 0;
	whereToRender.w = 0;
	for (auto character : text)
	{
		if (character <= 126 && character >= 33)
		{
			textureLocation.x = fontCharBounds[character].x;
			textureLocation.y = fontCharBounds[character].y;
			textureLocation.w = fontCharBounds[character].width;
			textureLocation.h = fontCharBounds[character].height;
			whereToRender.y = 100 - textureLocation.h + fontCharBounds[character].descender - fontCharBounds[character].raise;
			whereToRender.h = textureLocation.h;
			whereToRender.w = textureLocation.w;
			renderLocations.push_back(std::pair<SDL_Rect, SDL_Rect>(textureLocation, whereToRender));
			whereToRender.x += textureLocation.w + 3;
		}
	}
	return std::make_shared<SDLBitmapTextSprite>(fontTexture, mainRenderer, renderLocations);
}
*/


void SDLGraphicResourceFactory::RenderSprite(int ID, Transform transform)
{
	auto currentSpriteIterator = sprites.find(ID);
	if (ID < 0 || currentSpriteIterator == sprites.end()) return; // We have a bad sprite.
	SDL_Rect imageRect = { currentSpriteIterator->second.x, currentSpriteIterator->second.y, 
		currentSpriteIterator->second.width, currentSpriteIterator->second.height};
	SDL_Rect targetRect = {
		static_cast<int>(transform.location.x * static_cast<float>(windowWidth)),
		static_cast<int>(transform.location.y * static_cast<float>(windowHeight)),
		static_cast<int>(static_cast<float>(currentSpriteIterator->second.width) * transform.scale.x * xScale),
		static_cast<int>(static_cast<float>(currentSpriteIterator->second.height) * transform.scale.y * yScale)
	};
	
	SDL_RenderCopyEx(mainRenderer, currentSpriteIterator->second.texture, &imageRect, &targetRect, 
		transform.rotation, nullptr, SDL_FLIP_NONE);
}

SDLGraphicResourceFactory::SDLGraphicResourceFactory() :
mainWindow(nullptr),
mainRenderer(nullptr),
fontTexture(nullptr)
{

}

SDLGraphicResourceFactory::~SDLGraphicResourceFactory()
{
	SDLGraphicResourceFactory::Destroy();
}