#include "Graphics\Sprite\Sprite.h"

#include "Graphics\Factory\IGraphicResourceFactory.h"

Sprite::Sprite() : referenceID(-1), parent(nullptr)
{ 
	localTransform = Transform::Identity();
}

Sprite::Sprite(int refID, IGraphicResourceFactory* parent) : referenceID(refID), parent(parent)
{
	localTransform = Transform::Identity();
}

Sprite::Sprite(const Sprite& copy) : referenceID(copy.referenceID), parent(copy.parent)
{
	localTransform = Transform::Identity();
}


void Sprite::Render(Transform parentTransform)
{
	Transform newTransform = localTransform;
	parent->RenderSprite(referenceID, newTransform.Combine(parentTransform));
}