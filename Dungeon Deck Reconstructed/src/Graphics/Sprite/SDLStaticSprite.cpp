#include "Graphics\Sprite\SDLStaticSprite.h"

#include <SDL_image.h>

void SDLStaticSprite::ChangeDisplaySize(int width, int height)
{
	locationToDraw.w = width;
	locationToDraw.h = height;
}

void SDLStaticSprite::Render(int xLocation, int yLocation)
{
	locationToDraw.x = xLocation;
	locationToDraw.y = yLocation;
	SDL_RenderCopy(targetRenderer, texture, &imageDimensions, &locationToDraw);
}

SDLStaticSprite::SDLStaticSprite(SDL_Renderer* targetRenderer, SDL_Texture* texture, SDL_Rect imageDimensions) :
targetRenderer(targetRenderer),
texture(texture),
imageDimensions(imageDimensions)
{
	locationToDraw.x = 0;
	locationToDraw.y = 0;
	locationToDraw.h = imageDimensions.h;
	locationToDraw.w = imageDimensions.w;
}
