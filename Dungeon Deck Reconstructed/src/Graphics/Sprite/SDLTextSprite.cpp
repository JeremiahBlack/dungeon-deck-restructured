#include "Graphics\Sprite\SDLTextSprite.h"

SDLTextSprite::~SDLTextSprite()
{
	if (texture != nullptr)
	{
		SDL_DestroyTexture(texture);
		texture = nullptr;
	}
}