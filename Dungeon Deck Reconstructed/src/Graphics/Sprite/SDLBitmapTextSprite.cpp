#include "Graphics\Sprite\SDLBitmapTextSprite.h"

SDLBitmapTextSprite::SDLBitmapTextSprite(SDL_Texture* texture, SDL_Renderer* renderer, std::vector<std::pair<SDL_Rect, SDL_Rect> > listOfBounds) :
texture(texture),
renderer(renderer), 
letterBounds(listOfBounds)
{

}

void SDLBitmapTextSprite::Render(int xLocation, int yLocation)
{
	SDL_Rect renderLocation;
	for (auto pair : letterBounds)
	{
		renderLocation = pair.second;
		renderLocation.x += xLocation;
		renderLocation.y += yLocation;
		SDL_RenderCopy(renderer, texture, &pair.first, &renderLocation);
	}
}