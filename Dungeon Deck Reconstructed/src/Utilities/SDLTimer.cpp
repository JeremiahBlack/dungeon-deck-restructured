#include "Utilities\SDLTimer.h"

void SDLTimer::Start()
{
	startTime = SDL_GetTicks();
	isPaused = false;
}

void SDLTimer::Stop()
{
	startTime = pausedTime = 0;
	isPaused = true;
}

void SDLTimer::Pause()
{
	if (!isPaused)
	{
		pausedTime = SDL_GetTicks();
		isPaused = true;
	}
}

void SDLTimer::Resume()
{
	if (isPaused)
	{
		Uint32 pauseDifference = SDL_GetTicks() - pausedTime;
		startTime += pauseDifference;
		isPaused = false;
	}
}

unsigned int SDLTimer::GetCurrentMilliseconds()
{	
	return static_cast<unsigned int>(isPaused ? pausedTime : SDL_GetTicks() - startTime);
}