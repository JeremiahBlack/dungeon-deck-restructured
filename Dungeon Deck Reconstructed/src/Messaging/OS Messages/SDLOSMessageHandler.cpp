#include "Messaging\OS Messages\SDLOSMessageHandler.h"

#include "Messaging\OS Messages\Mouse\MouseMoveMessage.h"
#include "Messaging\OS Messages\Mouse\MouseButtonDownMessage.h"
#include "Messaging\OS Messages\Mouse\MouseButtonUpMessage.h"
#include "Messaging\OS Messages\Mouse\MouseWheelScrollMessage.h"
#include "Messaging\OS Messages\SDLKeyDownMessage.h"
#include "Messaging\OS Messages\SDLKeyUpMessage.h"
#include "Messaging\OS Messages\SDLQuitMessage.h"
#include "Messaging\OS Messages\Window\WindowResizeMessage.h"


void SDLOSMessageHandler::RunOSMessageLoop(std::shared_ptr<IMessageDistributor> distributor)
{
	std::shared_ptr<IMessage> messageToSend;
	while (SDL_PollEvent(&event))
	{
		messageToSend = nullptr;
		switch (event.type)
		{
		case SDL_KEYDOWN:
			messageToSend = std::make_shared<SDLKeyDownMessage>
				(event.key.keysym.sym, static_cast<SDL_Keymod>(event.key.keysym.mod), event.key.keysym.scancode);
			break;
		case SDL_KEYUP:
			messageToSend = std::make_shared<SDLKeyUpMessage>
				(event.key.keysym.sym, static_cast<SDL_Keymod>(event.key.keysym.mod), event.key.keysym.scancode);
			break;
		case SDL_MOUSEBUTTONDOWN:
			MouseButton buttonType;
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:
				buttonType = MouseButton::Left;
				break;
			case SDL_BUTTON_MIDDLE:
				buttonType = MouseButton::Middle;
				break;
			case SDL_BUTTON_RIGHT:
				buttonType = MouseButton::Right;
				break;
			default:
				buttonType = MouseButton::None;
				break;
			}
			messageToSend = std::make_shared < MouseButtonDownMessage >
				(buttonType);
			break;
		case SDL_MOUSEBUTTONUP:
			MouseButton button;
			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:
				button = MouseButton::Left;
				break;
			case SDL_BUTTON_MIDDLE:
				button = MouseButton::Middle;
				break;
			case SDL_BUTTON_RIGHT:
				button = MouseButton::Right;
				break;
			default:
				button = MouseButton::None;
				break;
			}
			messageToSend = std::make_shared < MouseButtonUpMessage >
				(buttonType);
			break;
		case SDL_MOUSEMOTION:
			messageToSend = std::make_shared<MouseMoveMessage>
				(event.motion.x, event.motion.y);
			break;
		case SDL_MOUSEWHEEL:
			messageToSend = std::make_shared<MouseWheelScrollMessage>
				(event.wheel.x, event.wheel.y);
			break;
		case SDL_WINDOWEVENT:
			if (event.window.event == SDL_WINDOWEVENT_RESIZED){
				messageToSend = std::make_shared < WindowResizeMessage >
					(event.window.data1, event.window.data2);
			}
			break;
		case SDL_QUIT:
			messageToSend = std::make_shared<SDLQuitMessage>();
			break;
		}
		if (messageToSend != nullptr)
		{
			distributor->DistributeMessage(messageToSend);
		}
	}
}