#include "Messaging\Distributors\BaseMessageDistributor.h"

#include <algorithm>

#include "Messaging\Listeners\BaseMessageListener.h"

void BaseMessageDistributor::DistributeMessage(std::shared_ptr<IMessage> message)
{
	auto mapIterator = subscribers.find(message->GetType());
	if (mapIterator != subscribers.end())
	{
		auto& messageListeners = mapIterator->second;
		for (auto listener : messageListeners)
		{
			listener->ReceiveMessage(message);
		}
	}
}

std::unique_ptr<IMessageListener> BaseMessageDistributor::CreateListener()
{

	return std::make_unique<BaseMessageListener>(this);
}

void BaseMessageDistributor::SubscribeListener(IMessageListener* listener, MessageType type)
{
	auto& subscriberVector = subscribers[type];
	
	if (std::find(subscriberVector.begin(), subscriberVector.end(), listener) 
		== subscriberVector.end())
	{
		// The subscriber doesn't exist yet!
		subscriberVector.push_back(listener);
	}
}

void BaseMessageDistributor::UnsubscribeListener(IMessageListener* listener, MessageType type)
{
	auto mapIterator = subscribers.find(type);
	if (mapIterator != subscribers.end())
	{
		// We know there's a vector for this type is already made.
		auto& vector = mapIterator->second;
		vector.erase(std::remove(vector.begin(), vector.end(), listener), vector.end());
	}
}