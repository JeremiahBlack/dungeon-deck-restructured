#include "Messaging\Listeners\BaseMessageListener.h"

void BaseMessageListener::AddSubscription(MessageType type)
{
	if (std::find(subscriptions.begin(), subscriptions.end(), type) == subscriptions.end())
	{
		parentDistributor->SubscribeListener(this, type);
		subscriptions.push_back(type);
	}
}

void BaseMessageListener::RemoveSubscription(MessageType type)
{
	parentDistributor->UnsubscribeListener(this, type);
	subscriptions.erase(std::remove(subscriptions.begin(), subscriptions.end(), type), subscriptions.end());
}

void BaseMessageListener::ReceiveMessage(std::shared_ptr<IMessage> message)
{
	messageQueue.push(message);
}

std::shared_ptr<IMessage> BaseMessageListener::PollMessage()
{
	if (!messageQueue.empty())
	{
		auto nextEvent = messageQueue.front();
		messageQueue.pop();

		return nextEvent;
	}
	return nullptr;
}

BaseMessageListener::BaseMessageListener(IMessageDistributor* parentDistributor) :
parentDistributor(parentDistributor)
{

}

BaseMessageListener::~BaseMessageListener()
{
	for (auto messageType : subscriptions)
	{
		RemoveSubscription(messageType);
	}
}