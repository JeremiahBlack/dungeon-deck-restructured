#include "Creatures\Attribute.h"

Attribute::Attribute(unsigned int value) : 
attributeValue(value), 
attributeMaxValue(value) 
{

}

void Attribute::DamageAttribute(unsigned int value)
{
	if (attributeValue < value)
	{
		attributeValue = 0;
	}
	else
	{
		attributeValue -= value;
	}
}

void Attribute::HealAttribute(unsigned int value)
{
	attributeValue += value;
	if (attributeValue > attributeMaxValue)
	{
		attributeValue = attributeMaxValue;
	}
}

void Attribute::IncreaseMaxValue(unsigned int increaseValue)
{
	attributeMaxValue += increaseValue;
}
