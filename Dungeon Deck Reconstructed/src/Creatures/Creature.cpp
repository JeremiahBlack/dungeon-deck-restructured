#include "Creatures\Creature.h"

Creature::Creature(std::string name,
	unsigned int health, unsigned int defense, unsigned int attack) :
name(name),
health(health),
defense(defense),
attack(attack)
{

}

bool Creature::IsDead()
{
	return health.GetCurrentValue() == 0;
}

void Creature::Attack(Creature& target)
{
	if (target.defense.GetCurrentValue() < attack.GetCurrentValue())
	{
		target.health.DamageAttribute(attack.GetCurrentValue() - target.GetDefense().GetCurrentValue());
	}
}