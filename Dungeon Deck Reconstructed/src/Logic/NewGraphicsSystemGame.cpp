#include "Logic\NewGraphicsSystemGame.h"

#include "Messaging\OS Messages\SDLKeyDownMessage.h"

NewGraphicsSystemGame::NewGraphicsSystemGame(std::shared_ptr<IGraphicResourceFactory> graphicsFactory,
	std::shared_ptr<IMessageDistributor> osMessageDistributor)
{
	isRunning = true;
	osMessageListener = osMessageDistributor->CreateListener();
	osMessageListener->AddSubscription(SDLKeyDownMessage::Type);
	testSprite = graphicsFactory->CreateSprite("Images\\Brick Background.png", 0, 0, 1920, 1080);
}

void NewGraphicsSystemGame::Update()
{
	while (message = osMessageListener->PollMessage())
	{
		std::shared_ptr<SDLKeyDownMessage> castMessage = std::dynamic_pointer_cast<SDLKeyDownMessage>(message);
		if (castMessage->GetKeyCode() == SDLK_ESCAPE)
		{
			isRunning = false;
		}
	}
}

void NewGraphicsSystemGame::Render()
{
	testSprite.Render();
}