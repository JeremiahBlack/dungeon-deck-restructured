#include "Logic\Game.h"

#include <assert.h>

#include "Logic\Game States\TestingGameState.h"

Game::Game(std::shared_ptr<IGraphicResourceFactory> GraphicsFactory, std::shared_ptr<IMessageDistributor> OSMessageDistributor) :
isRunning(true),
graphicsFactory(GraphicsFactory),
osMessageDistributor(OSMessageDistributor),
currentState(nullptr)
{

}

Game::~Game()
{
	Destroy();
}

bool Game::Initialize()
{
	currentState = new TestingGameState(graphicsFactory, osMessageDistributor);
	return currentState->Initialize();
}

void Game::Destroy()
{
	if (currentState)
	{
		currentState->Destroy();
		delete currentState;
		currentState = nullptr;
	}
}

void Game::Update()
{
	assert(currentState);
	currentState->Update();
}

void Game::Render()
{
	currentState->Render();
}