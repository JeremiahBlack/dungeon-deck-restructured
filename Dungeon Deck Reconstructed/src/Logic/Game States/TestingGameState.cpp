#include "Logic\Game States\TestingGameState.h"

TestingGameState::TestingGameState(std::shared_ptr<IGraphicResourceFactory> GraphicFactory, std::shared_ptr<IMessageDistributor> OSMessageDistributor) :
GameState(GraphicFactory, OSMessageDistributor)
{

}

bool TestingGameState::Initialize()
{
	background = graphicFactory->CreateSprite("Images\\Brick Background.png", 0, 0, 1920, 1080);
	return true;
}

void TestingGameState::Destroy()
{
	// Nothing to do here.
}

void TestingGameState::Pause()
{
	// Nothing to do here.
}

void TestingGameState::Resume()
{
	// Nothing to do here.
}

void TestingGameState::Update()
{
	// Nothing to do here.
}

void TestingGameState::Render()
{
	background.Render();
}