#include "Logic\Game Objects\GameObjectFactory.h"
#include "Logic\Game Objects\Components\GameComponent.h"
#include "Logic\Game Objects\Components\RenderableComponent.h"

GameObject* GameObjectFactory::CreateNewGameObject(Json::Value information)
{
	/* -- Format
		{
			"Components" : [
			{
			"Type" : 1 // Specific ID of component type.
			}
			]
		}
	*/
	for (auto component : information["Components"])
	{
		unsigned int componentType = component.get("Type", 0).asUInt();
		switch (componentType)
		{
		case 0: // BAD ID.
			break;
		case 1: // Don't know at this time.
			break;
		}
	}
	GameObject* newObject = new GameObject(currentID);
	++currentID;
	return newObject;
}